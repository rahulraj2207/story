import 'dart:convert';
import 'package:get/state_manager.dart';
import 'package:story/model/listModel.dart';
import 'package:story/services/listApi.dart';
import 'package:get/get.dart';

var data ="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjIxNDAzNzAzLCJqdGkiOiJiZjQ5MmU5NTZjMjA0ZjUwOWMxYmUwYmQ2MTY0MTQ5NCIsInVzZXJfaWQiOjJ9.FotJDK1vyREWzZza_HCqZjYZNmVoBsqiGC5dq5Rg7tU";

class ListController extends GetxController {
  RxBool isLoading = true.obs;
  var storyList = <StoryList>[].obs;

  // RxList<StoryList> storyList = <StoryList>[].obs;

  @override
  void onInit() {
    listFetchApi();
    super.onInit();
  }
  void listFetchApi() async {
    try {
      isLoading(true);
      await ListApi().fetchListApi(token: data).then((response) {
        if (response.statusCode == 200) {
          print('listscccss');

          var events2 = storyListFromJson(response.body);
          storyList.addAll(events2);
        } else {
          print('error occured in events details Page');
        }
      });
    }finally{
      isLoading(false);
    }
  }
}

