import 'package:get/state_manager.dart';
import 'package:story/services/viewsApi.dart';
import 'package:get/get.dart';
import 'package:story/model/viewsModel.dart';

class ViewController extends GetxController {
  RxBool isLoading = true.obs;
  var viewsData = <ViewsModel>[].obs;
  @override
  void onInit() {
    print("contr");

    print(viewsData.length);
    viewsFetchApi();
    super.onInit();
  }
  int storyId=11;
  void viewsFetchApi() async {
    try {
      isLoading(true);
      await ViewsApi().fetchViewsApi(storyId: storyId).then((response) {
        if (response.statusCode == 200) {
          var events2 = viewsModelFromJson(response.body);
          viewsData.addAll(events2);

          print('view controller $events2');
        } else {
          print('error occured in events details Page');
        }
      });
    }finally{
      isLoading(false);
    }
  }
}

