// import 'dart:convert';
//
// import 'package:get/get.dart';
// // import 'package:mutualEvents/controller/UserDataController.dart';
// // import 'package:mutualEvents/screens/Home/models/event_selection_model.dart';
// // import 'package:mutualEvents/screens/Home/services/event_selection_api.dart';
//
// class EventSelectionController extends GetxController {
//   RxBool isLoading = true.obs;
//   RxList<EventSelectionModel> eventselection = <EventSelectionModel>[].obs;
//
//   String evenid;
//
//   @override
//   void onInit() {
//     fetchEventSelection();
//     super.onInit();
//   }
//
//   @override
//   void onClose() {
//     // TODO: implement onClose
//     super.onClose();
//   }
//
//   void fetchEventSelection() async {
//     try {
//       isLoading(true);
//       await EventSelectionApi()
//           .fetchEventSelection(
//         token: Get.find<UserDataController>().accessTokenModel.access,
//       )
//           .then((response) {
//         if (response.statusCode == 200) {
//           var events = eventSelectionModelFromJson(response.body);
//           eventselection.assignAll(events);
//         } else {
//           print('event selection error');
//         }
//       });
//     } finally {
//       isLoading(false);
//     }
//   }
// }
