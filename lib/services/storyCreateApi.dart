import 'package:http/http.dart' as http;
import 'dart:io';

class CreateApi {
  String token =
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjIxMzE4ODU5LCJqdGkiOiI4YzA4NjBhZGJjMTU0MDYwODJmNDUzMThkNzA2MTA5NCIsInVzZXJfaWQiOjR9.JL8JN7OxLHJCSTnvvOmWHDzC9jDDS7sQhrQ5VLIclr0";

  Future<http.Response> sendimage({
    required int event,
    required String description,
    required File imageFile,
  }) async {
    print('................entred editCreate');
    http.Response response;

    try {
      var request = http.MultipartRequest(
          'POST',
          Uri.parse(
              'https://51c046c6-73be-447a-a38c-d999464b1b85.mutualevents.co/api/v1/story/create/'))
        ..fields['event'] = event.toString()
        ..fields['description'] = description
        ..files.add(
          http.MultipartFile('story', imageFile.readAsBytes().asStream(),
              imageFile.lengthSync(),
              filename: imageFile.path.split('/').last),
        )
        ..headers.addAll({'Authorization': 'Bearer $token'});
      response = await http.Response.fromStream(await request.send());
      print(response.body);
      return response;
    } catch (e) {
      print('-------error in editdp ${e}');
      throw 'throw response check create edit api';
    }
  }
}
