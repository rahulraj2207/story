import 'package:flutter/material.dart';
import 'package:story_view/story_view.dart';
import 'package:story/controller/listController.dart';
import 'package:get/get.dart';





class DisplayStory extends StatefulWidget {
  @override
  _DisplayStoryState createState() => _DisplayStoryState();
}

class _DisplayStoryState extends State<DisplayStory> {

  final storyController = StoryController();
  final listController = Get.put(ListController());

  List<StoryItem> stories = [];

  @override
  void initState() {
    super.initState();
  //     setState(() {
  // SizedBox(
  //   height: 300,
  //   child: Obx(() {
  //     // if (listController.storyList.isEmpty)
  //     //   return Center(child: CircularProgressIndicator());
  //     // else
  //     return ListView.builder(
  //       itemCount: 3,
  //       // listController.storyList.length,
  //       itemBuilder: (BuildContext context, int index) {
  //         return StoryView(
  //           storyItems: [
  //             StoryItem.text(
  //               title: "I guess you'd love to see more of our food. That's great.",
  //               backgroundColor: Colors.blue,
  //             ),
  //             StoryItem.pageImage(
  //               url: listController.storyList[index].story.toString(),
  //               caption: listController.storyList[index].description,
  //               controller: storyController,
  //             ),
  //
  //
  //           ],
  //           progressPosition: ProgressPosition.top,
  //           repeat: false,
  //           controller: storyController,
  //         );
  //       },
  //     );
  //   }
  //   ),
  // );
  //
  //     });

  }



  @override
  void dispose() {
    storyController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:  Obx((){
        if (listController.storyList.isEmpty)
          return Center(child: CircularProgressIndicator());
        else
          return ListView.builder(
            itemCount:
            listController.storyList.length,
            itemBuilder: (BuildContext context, int index) {


              return StoryView(
                storyItems: [
                  StoryItem.text(
                    title: "I guess you'd love to see more of our food. That's great.",
                    backgroundColor: Colors.blue,
                  ),
                  StoryItem.text(
                    title: "Nice!\n\nTap to continue.",
                    backgroundColor: Colors.red,
                    textStyle: TextStyle(
                      fontFamily: 'Dancing',
                      fontSize: 40,
                    ),
                  ),
                  StoryItem.pageImage(
                    url: listController.storyList[index].story.toString(),
                    caption: listController.storyList[index].description,
                    controller: storyController,
                  ),


                ],
                onStoryShow: (s) {
                  print("Showing a story");
                },
                onComplete: () {
                  print("Completed a cycle");
                },
                progressPosition: ProgressPosition.top,
                repeat: false,
                controller: storyController,
              );


            },
          );
      }
      ),



    );
  }
}