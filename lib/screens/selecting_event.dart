// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// // import 'package:mutualEvents/screens/Home/Screens/uploadimagevideo/chooseLocationWriteDescription/chooseLocation_description.dart';
// // import 'package:mutualEvents/screens/Home/Screens/uploadimagevideo/descriptionShare.dart';
// // import 'package:mutualEvents/screens/Home/controllers/event_selection_controller.dart';
// import 'package:story/utils/button_widget.dart';
// import 'package:story/utils/theme.dart';
//
//
// class SelectingEvent extends StatelessWidget {
//   // SelectingEvent({Key key}) : super(key: key);
//
//   RxInt ind = 0.obs;
//
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       decoration: buildBoxDecoration(),
//       height: Get.height / 2.2,
//       child: Padding(
//         padding: const EdgeInsets.all(15),
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             Align(alignment: Alignment.topCenter),
//             GetX<EventSelectionController>(builder: (_) {
//               return _.isLoading.value
//                   ? const Center(
//                       child: CircularProgressIndicator(),
//                     )
//                   : SizedBox(
//                       height: Get.width / 2,
//                       child: ListView.builder(
//                           scrollDirection: Axis.horizontal,
//                           itemCount: _.eventselection.length,
//                           itemBuilder: (__, index) {
//                             var event = _.eventselection;
//
//                             String catename = event[index].category[0].name;
//
//                             String name = event[index].firstname +
//                                 _.eventselection[index].surname;
//
//                             _.evenid = event[0].id.toString();
//
//                             String eventimage;
//                             if (event[index].category == null) {
//                               eventimage = event[index].eventImage[0].image;
//                             } else {
//                               eventimage = event[index].category[0].image;
//                             }
//
//                             return InkWell(
//                               onTap: () {
//                                 ind.value = index;
//                                 _.evenid =
//                                     _.eventselection[index].id.toString();
//                                 print(_.evenid);
//                               },
//                               child: EventShow(
//                                   eventimage: eventimage,
//                                   catename: catename,
//                                   name: name,
//                                   index: index,
//                                   ind: ind),
//                             );
//                           }),
//                     );
//             }),
//             const Spacer(),
//             Row(
//               children: [
//                 Expanded(
//                   child: buttonWidget(
//                       height: 55,
//                       width: 55,
//                       // size.width,
//                       color: cPrimary,
//                       radius: 10,
//                       text: 'Done',
//                       press: () {
//                         Get.back();
//                         Get.to(LocationAndDesc());
//                       }),
//
//                   // SizedBox(
//                   //   height: 60,
//                   //   child: TextButton(
//                   //     style: ButtonStyle(
//                   //       backgroundColor:
//                   //           MaterialStateProperty.all(Colors.deepOrange),
//                   //       shape:
//                   //           MaterialStateProperty.all<RoundedRectangleBorder>(
//                   //         RoundedRectangleBorder(
//                   //           borderRadius: BorderRadius.circular(10.0),
//                   //         ),
//                   //       ),
//                   //     ),
//                   //     onPressed: () {
//                   //       Get.back();
//                   //       Get.to(LocationAndDesc());
//                   //     },
//                   //     child: const Text(
//                   //       'Done',
//                   //       style: TextStyle(color: Colors.white),
//                   //     ),
//                   //   ),
//                   // ),
//                 ),
//               ],
//             )
//           ],
//         ),
//       ),
//     );
//   }
// }
//
// class EventShow extends StatelessWidget {
//   const EventShow({
//     Key key,
//     @required this.eventimage,
//     @required this.catename,
//     @required this.name,
//     @required this.index,
//     @required this.ind,
//   }) : super(key: key);
//
//   final String eventimage;
//   final String catename;
//   final String name;
//   final int index;
//   final RxInt ind;
//
//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.all(15),
//       child: Obx(
//         () => Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: [
//             Stack(
//               children: [
//                 AnimatedContainer(
//                   duration: const Duration(milliseconds: 200),
//                   height: Get.width * 0.25,
//                   width: Get.width * 0.25,
//                   decoration: BoxDecoration(
//                       shape: BoxShape.circle,
//                       border: Border.all(
//                         color: ind.value == index ? cPrimary : cWhite,
//                       )),
//                   child: Padding(
//                     padding: ind.value == index
//                         ? const EdgeInsets.all(3)
//                         : const EdgeInsets.all(3),
//                     child: Container(
//                       clipBehavior: Clip.antiAlias,
//                       decoration: const BoxDecoration(
//                         shape: BoxShape.circle,
//                       ),
//                       child: CachedNetworkImage(
//                         imageUrl: eventimage,
//                         fit: BoxFit.cover,
//                         filterQuality: FilterQuality.none,
//                       ),
//                     ),
//                   ),
//                 ),
//                 ind.value == index
//                     ? AnimatedOpacity(
//                         opacity: ind.value == index ? 1 : 0,
//                         duration: const Duration(milliseconds: 200),
//                         child: Container(
//                           decoration: const BoxDecoration(
//                             color: Colors.white,
//                             shape: BoxShape.circle,
//                           ),
//                           child: Padding(
//                             padding: const EdgeInsets.all(3),
//                             child: Container(
//                               decoration: BoxDecoration(
//                                 color: cPrimary,
//                                 shape: BoxShape.circle,
//                               ),
//                               child: const Icon(
//                                 Icons.check,
//                                 size: 18,
//                                 color: Colors.white,
//                               ),
//                             ),
//                           ),
//                         ),
//                       )
//                     : Container(),
//               ],
//             ),
//             // CircleAvatar(
//             //   radius: 35,
//             //   backgroundColor: cPrimary,
//             //   backgroundImage: NetworkImage(eventimage),
//             // ),
//             // const SizedBox(height: 10),
//             Text(catename),
//             const SizedBox(height: 3),
//             Text(
//               name,
//               style: const TextStyle(
//                 color: Colors.grey,
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
