import 'dart:io';

import 'package:flutter/material.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:story/controller/storyCreateController.dart';
import 'package:get/get.dart';
import 'package:story/screens/storyViews.dart';


final StoryCreateController storyCreateController = Get.put(StoryCreateController());

 TextEditingController textEditingController =TextEditingController();
late String valueText;
 String codeDialog ="";
late File file1;
List<Asset> images = <Asset>[];
List<Asset> images1 = <Asset>[];

class Gallery extends StatefulWidget {

@override
  _GalleryState createState() => _GalleryState();
}


class _GalleryState extends State<Gallery> {

  late Future<File> imageFile;



  Future<void> _displayTextInputDialog() async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Add a caption'),
            content: TextField(
              onChanged: (value) {
                setState(() {
                  valueText = value;
                });
              },
              controller: textEditingController,
              decoration: InputDecoration(hintText: "Description"),
            ),
            actions: <Widget>[
              FlatButton(
                color: Colors.green,
                textColor: Colors.white,
                child: Text('OK'),
                onPressed: () {
                  setState(() {
                    codeDialog = valueText;
                    Navigator.pop(context);
                  });
                },
              ),

            ],
          );
        });
  }


  String _error = 'No Error Dectected';
  Future<void> loadAssets() async {
    List<Asset> resultList = <Asset>[];
    String error = 'No Error Detected';

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 300,
        enableCamera: true,
        selectedAssets: images,
        cupertinoOptions: const CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: const MaterialOptions(
          actionBarColor: '#FF5917',
          statusBarColor: '#000000',
          actionBarTitle: 'Post',
          allViewTitle: 'All Photos',
          useDetailsView: false,
          selectCircleStrokeColor: '#ffffff',
        ),
      );
    } on Exception catch (e) {
      error = e.toString();
    }


    setState(
            () {
          images = resultList;
          images1= resultList;

          images.forEach((asset) async {
            final byteData = await asset.getByteData();
            final tempFile =
            File('${(await getTemporaryDirectory()).path}/${asset.name}');
            final file = await tempFile.writeAsBytes(
              byteData.buffer
                  .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes),
            );
            print("files$file");
            print(tempFile.lengthSync());
            file1=file;
          });
          _error = error;
        });

    // filterimgCtr.images.clear();
}

@override
  void initState() {
loadAssets();
super.initState();
  }

  @override
  Widget build(BuildContext context) {

    final size = MediaQuery.of(context).size;


      return Stack(
      children: [

        // Align(alignment: Alignment.center,
        //   child: SingleChildScrollView(scrollDirection: Axis.horizontal,
        //     child: Row(
        //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //       children:
        //       List.generate(images1.length, (index) {
        //         Asset asset = images1[index];
        //         return AssetThumb(
        //           asset: asset,
        //           width: size.width.toInt(),
        //           height: size.height.toInt(),
        //         );
        //       }
        //       ),),
        //   ),
        // ),

        // Align(
        //   alignment: Alignment.bottomCenter,
        //   child: Expanded(
        //     child: GridView.count(
        //       crossAxisCount: 4,
        //       children: List.generate(images.length, (index) {
        //         Asset asset = images[index];
        //         return AssetThumb(
        //           asset: asset,
        //           width: 300,
        //           height: 300,
        //         );
        //       }),
        //     ),
        //   ),
        // ),








        Padding(
          padding: const EdgeInsets.only(left: 15.0,right: 130.0,bottom: 28),
          child: Align(alignment: Alignment.bottomLeft,
            child: SingleChildScrollView(scrollDirection: Axis.horizontal,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children:
                List.generate(images.length, (index) {

                      Asset asset = images[index];
                      return GestureDetector(
                        onTap: (){



                        },
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: AssetThumb(
                            asset: asset,
                            width: 60,
                            height: 60,
                          ),
                        ),
                      );
                    }
      ),),
            ),
          ),
        ),




        SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(top: 20.0,left: 20.0),
            child: Align(alignment: Alignment.topLeft,
              child: GestureDetector(
                onTap: (){
                  Get.back();
                },
                child: CircleAvatar(
                  maxRadius: 24,
                  backgroundColor: Colors.black.withOpacity(.3),
                  child: Icon(Icons.close,color: Colors.white),

                ),
              ),
            ),
          ),
        ),
        Column(
          children: [

            SafeArea(
              child: Padding(
                padding: const EdgeInsets.only(top: 20.0,right: 20.0),
                child: Align(alignment: Alignment.topRight,
                  child: GestureDetector(
                    onTap: (){

                    },
                    child: CircleAvatar(
                      maxRadius: 24,
                      backgroundColor: Colors.black.withOpacity(.3),
                      child: Icon(Icons.edit,color: Colors.white),

                    ),
                  ),
                ),
              ),
            ),
            SafeArea(
              child: Padding(
                padding: const EdgeInsets.only(right: 20.0),
                child: Align(alignment: Alignment.topRight,
                  child: GestureDetector(
                    onTap: (){
                      _displayTextInputDialog();


                    },
                    child: CircleAvatar(
                      maxRadius: 24,
                      backgroundColor: Colors.black.withOpacity(.3),
                      child: Icon(Icons.text_fields,color: Colors.white),

                    ),
                  ),
                ),
              ),
            ),
            SafeArea(
              child: Padding(
                padding: const EdgeInsets.only(right: 20.0),
                child: Align(alignment: Alignment.topRight,
                  child: GestureDetector(
                    onTap: (){

                    },
                    child: CircleAvatar(
                      maxRadius: 24,
                      backgroundColor: Colors.black.withOpacity(.3),
                      child: Icon(
                        Icons.calendar_today,color: Colors.white,),

                    ),
                  ),
                ),
              ),
            ),



          ],
        ),

        Padding(
          padding: const EdgeInsets.only(right: 15.0,bottom: 30),
          child: Align(alignment: Alignment.bottomRight,
            child: MaterialButton(onPressed: (){
              storyCreateController.editStoryApi(codeDialog, file1);
              Get.to(StoryViews());



            },
              color: Colors.deepOrange,
              textColor: Colors.white,
              child: Text(
                "Done"
              ),
              shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(13.0),
           ),

            ),
          ),
        )

    ]
    );

  }
}






