import 'dart:async';
import 'dart:io';
import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:story/screens/gallery.dart';
import 'dart:ui' as ui;
import 'package:flutter/rendering.dart';
import 'package:story/controller/storyCreateController.dart';

var a = false;

class TakeOrUpload extends StatefulWidget {
  final CameraDescription camera;

  const TakeOrUpload({
    required this.camera,
  });
  @override
  TakeOrUploadState createState() => TakeOrUploadState();
}

class TakeOrUploadState extends State<TakeOrUpload> {
  late File image;
  bool b = false;

  late CameraController _controller;
  late Future<void> _initializeControllerFuture;

  @override
  void initState() {
    super.initState();
    // To display the current output from the Camera,
    // create a CameraController.
    _controller = CameraController(
      // Get a specific camera from the list of available cameras.
      widget.camera,
      // Define the resolution to use.
      ResolutionPreset.medium,
    );
    // Next, initialize the controller. This returns a Future.
    _initializeControllerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    // Dispose of the controller when the widget is disposed.
    _controller.dispose();
    super.dispose();
  }

  Widget cameraWidget(context) {
    var camera = _controller.value;
    final size = MediaQuery.of(context).size;
    var scale = size.aspectRatio * camera.aspectRatio;
    if (scale < 1) scale = 1 / scale;

    return Transform.scale(
        scale: scale, child: Center(child: CameraPreview(_controller)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // Wait until the controller is initialized before displaying the
      // camera preview. Use a FutureBuilder to display a loading spinner
      // until the controller has finished initializing.
      body: FutureBuilder<void>(
        future: _initializeControllerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            // If the Future is complete, display the preview.
            return Stack(children: [
              Align(alignment: Alignment.center, child: cameraWidget(context)),
              SafeArea(
                child: Padding(
                  padding: const EdgeInsets.only(top: 20.0, left: 20.0),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: GestureDetector(
                      onTap: () {
                        Get.back();
                      },
                      child: CircleAvatar(
                        maxRadius: 24,
                        backgroundColor: Colors.black.withOpacity(.3),
                        child: Icon(
                          Icons.close,
                          color: Colors.white,
                          size: 20,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(23.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: MaterialButton(
                        onPressed: () {
                          Get.to(Gallery());
                        },
                        textColor: Colors.white,
                        color: Colors.black.withOpacity(.3),
                        child: Icon(
                          CupertinoIcons.camera,
                          size: 20,
                        ),
                        padding: EdgeInsets.all(14),
                        shape: CircleBorder(),
                      ),
                    ),

                    ClipOval(
                      child: Material(
                        borderOnForeground: true,
                        color: Colors.black.withOpacity(.3), // button color
                        child: InkWell(
                          splashColor:
                              Colors.grey.withOpacity(.5), // inkwell color
                          child: SizedBox(
                            width: 90,
                            height: 95,
                          ),
                          onTap: () async {
                            try {
                              b = true;
                              await _initializeControllerFuture;
                              final image = await _controller.takePicture();
                              File file = File(image.path);
                              // storyCreateController.editStoryApi("description", file);
                              Get.to(DisplayPictureScreen(imagePath: file));
                            } catch (e) {
                              print(e);
                            }
                          },
                        ),
                      ),
                    ),


                    Align(
                      alignment: Alignment.bottomRight,
                      child: MaterialButton(
                        onPressed: () {},
                        color: Colors.black.withOpacity(.3),
                        textColor: Colors.white,
                        child: Icon(
                          CupertinoIcons.camera_rotate,
                          size: 20,
                        ),
                        padding: EdgeInsets.all(14),
                        shape: CircleBorder(),
                      ),
                    )
                  ],
                ),
              )
            ]);
          } else {
            // Otherwise, display a loading indicator.
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}

// A widget that displays the picture taken by the user.
class DisplayPictureScreen extends StatefulWidget {
  final File imagePath;
  // final String description;

  const DisplayPictureScreen({required this.imagePath});

  @override
  _DisplayPictureScreenState createState() => _DisplayPictureScreenState();
}

final StoryCreateController storyCreateController =
    Get.put(StoryCreateController());

class _DisplayPictureScreenState extends State<DisplayPictureScreen> {
  GlobalKey globalKey = GlobalKey();

  List<TouchPoints> points = [];
  double opacity = 1.0;
  StrokeCap strokeType = StrokeCap.round;
  double strokeWidth = 5.0;
  Color selectedColor = Colors.yellowAccent;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      // The image is stored as a file on the device. Use the `Image.file`
      // constructor with the given path to display the image.
      body: GestureDetector(
        onPanUpdate: (details) {
          setState(() {
            RenderBox? renderBox = context.findRenderObject() as RenderBox?;
            points.add(TouchPoints(
                points: renderBox!.globalToLocal(details.globalPosition),
                paint: Paint()
                  ..strokeCap = strokeType
                  ..isAntiAlias = true
                  ..color = selectedColor.withOpacity(opacity)
                  ..strokeWidth = strokeWidth));
          });
        },
        onPanStart: (details) {
          setState(() {
            RenderBox? renderBox = context.findRenderObject() as RenderBox?;
            points.add(TouchPoints(
                points: renderBox!.globalToLocal(details.globalPosition),
                paint: Paint()
                  ..strokeCap = strokeType
                  ..isAntiAlias = true
                  ..color = selectedColor.withOpacity(opacity)
                  ..strokeWidth = strokeWidth));
          });
        },
        onPanEnd: (details) {
          setState(() {
            // points.add();
          });
        },
        child: RepaintBoundary(
          key: globalKey,
          child: Stack(
            children: <Widget>[
              Container(
                width: size.width,
                height: size.height,
                child: Image.file(widget.imagePath),
              ),
              CustomPaint(
                size: Size.infinite,
                painter: MyPainter(
                  pointsList: points,
                ),
              ),
              SafeArea(
                child: Padding(
                  padding: const EdgeInsets.only(top: 20.0, left: 20.0),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: GestureDetector(
                      onTap: () {
                        Get.back();
                      },
                      child: CircleAvatar(
                        maxRadius: 24,
                        backgroundColor: Colors.black.withOpacity(.3),
                        child: Icon(
                          Icons.close,
                          color: Colors.white,
                          size: 19,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Column(
                children: [
                  SafeArea(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 20.0, right: 20.0),
                      child: Align(
                        alignment: Alignment.topRight,
                        child: GestureDetector(
                          onTap: () {},
                          child: CircleAvatar(
                            maxRadius: 24,
                            backgroundColor: Colors.black.withOpacity(.3),
                            child: Icon(
                              Icons.edit,
                              color: Colors.white,
                              size: 19,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SafeArea(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 20.0),
                      child: Align(
                        alignment: Alignment.topRight,
                        child: GestureDetector(
                          onTap: () {},
                          child: CircleAvatar(
                            maxRadius: 24,
                            backgroundColor: Colors.black.withOpacity(.3),
                            child: Icon(
                              Icons.text_fields,
                              color: Colors.white,
                              size: 19,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SafeArea(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 20.0),
                      child: Align(
                        alignment: Alignment.topRight,
                        child: GestureDetector(
                          onTap: () {},
                          child: CircleAvatar(
                            maxRadius: 24,
                            backgroundColor: Colors.black.withOpacity(.3),
                            child: Icon(
                              Icons.calendar_today,
                              color: Colors.white,
                              size: 19,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SafeArea(
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 30, right: 20.0),
                  child: Align(
                    alignment: Alignment.bottomRight,
                    child: GestureDetector(
                      onTap: () {
                        storyCreateController.editStoryApi(
                            "description", widget.imagePath);
                      },
                      child: CircleAvatar(
                        maxRadius: 28,
                        backgroundColor: Colors.deepOrange,
                        child: Icon(
                          Icons.send,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}


class MyPainter extends CustomPainter {
  MyPainter({required this.pointsList});

  //Keep track of the points tapped on the screen
  List<TouchPoints> pointsList;
  List<Offset> offsetPoints = [];

  //This is where we can draw on canvas.
  @override
  void paint(Canvas canvas, Size size) {
    for (int i = 0; i < pointsList.length - 1; i++) {
      if (pointsList[i] != null && pointsList[i + 1] != null) {
        //Drawing line when two consecutive points are available
        canvas.drawLine(pointsList[i].points, pointsList[i + 1].points,
            pointsList[i].paint);
      } else if (pointsList[i] != null && pointsList[i + 1] == null) {
        offsetPoints.clear();
        offsetPoints.add(pointsList[i].points);
        offsetPoints.add(Offset(
            pointsList[i].points.dx + 0.1, pointsList[i].points.dy + 0.1));

        //Draw points when two points are not next to each other
        canvas.drawPoints(
            ui.PointMode.points, offsetPoints, pointsList[i].paint);
      }
    }
  }

  //Called when CustomPainter is rebuilt.
  //Returning true because we want canvas to be rebuilt to reflect new changes.
  @override
  bool shouldRepaint(MyPainter oldDelegate) => true;
}

//Class to define a point touched at canvas
class TouchPoints {
  Paint paint;
  Offset points;
  TouchPoints({required this.points, required this.paint});
}
