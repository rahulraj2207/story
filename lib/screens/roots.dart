import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:story/screens/takeOrUpload.dart';
import 'package:camera/camera.dart';
import 'package:story/screens/displayStory.dart';
import 'package:story/screens/demo.dart';

Future<void> getCamera() async {
  WidgetsFlutterBinding.ensureInitialized();

  final cameras = await availableCameras();


  final firstCamera = cameras.first;

  if (cameras.length > 0) {
    final secondCamera = cameras[1];
  }
}


class FirstRoute extends StatelessWidget {



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Center(
            child: Text(
              'Task routes',
              style: TextStyle(color: Colors.black),
            )),
      ),
      body: Center(
        child: ListView(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 50),


                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: ConstrainedBox(
                    constraints: BoxConstraints.tightFor(width: 150, height: 50),
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.deepOrange),
                      ),
                      child: Text('Story add'),
                      onPressed: () {



                        Future<void> getCamera() async {

                          WidgetsFlutterBinding.ensureInitialized();

                          final cameras = await availableCameras();

                          final firstCamera = cameras.first;


                              Get.to(TakeOrUpload(camera: firstCamera));

                        }
                        getCamera();





                      },
                    ),
                  ),
                ),

                SizedBox(height: 50),

                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: ConstrainedBox(
                    constraints: BoxConstraints.tightFor(width: 150, height: 50),
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.deepOrange),
                      ),
                      child: Text('story demo'),
                      onPressed: () {
                        Get.to(MoreStories());
                      },
                    ),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: ConstrainedBox(
                    constraints: BoxConstraints.tightFor(width: 150, height: 50),
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.deepOrange),
                      ),
                      child: Text('story'),
                      onPressed: () {
                        Get.to(DisplayStory());
                      },
                    ),
                  ),
                ),




              ],
            )
          ],
        ),
      ),
    );
  }
}
