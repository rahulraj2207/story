import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:story/controller/viewController.dart';

late int views=2;
class StoryViews extends StatefulWidget {
  // final int views;
  // const StoryViews({
  //   required this.views,
  // }) ;

  @override
  _StoryViewsState createState() => _StoryViewsState();
}

class _StoryViewsState extends State<StoryViews> {
  final ViewController viewController =Get.put(ViewController());

 @override
  void initState() {
super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Center(
            child: Text(
              'Views',
              style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),
            )),
        actions: <Widget>[
         MaterialButton(
           onPressed: (){

         },child: Text("Done",style: TextStyle(
           fontSize: 16,
           fontWeight: FontWeight.bold,
           color: Colors.deepOrange,
         ),
         ),
              color: Colors.white,
         )
        ],


      ) ,
      body: (views==0)?

         Column(mainAxisAlignment: MainAxisAlignment.center,
          children: [


            Align(alignment: Alignment.center,
                child: Icon(CupertinoIcons.eye_slash,size: 30,color: Colors.deepOrange,)),
            SizedBox(height: 30,),
            Text("Viewed by 0",style: TextStyle(
              color: Colors.black,fontSize: 20,fontWeight: FontWeight.w500
            ),),
            SizedBox(height: 20,),

            Text("Ahh... it seems that your story has no",style: TextStyle(
                color: Colors.black.withOpacity(.3),fontSize: 15,fontWeight: FontWeight.w500
            )),
            SizedBox(height: 10,),

            Text(" views yet.",style: TextStyle(
                color: Colors.black.withOpacity(.3),fontSize: 15,fontWeight: FontWeight.w500
            )),

          ],
        ):
      Column(mainAxisAlignment: MainAxisAlignment.end,

        children: [
          GestureDetector(
            onTap: (){
              Get.bottomSheet(
                ClipRRect(
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20)),
                  child: ColoredBox(
                    color: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          ClipRRect(
                            borderRadius:
                            BorderRadius.circular(10),
                            child: ColoredBox(
                              color: Colors.grey
                                  .withOpacity(0.3),
                              child: const SizedBox(
                                height: 5,
                                width: 40,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets
                                .symmetric(
                                vertical: 12.0,
                                horizontal: 4),
                            child: Text("Viewed by $views",style: TextStyle(
                                color: Colors.black.withOpacity(.9),fontSize: 20,fontWeight: FontWeight.w500
                            ),),
                          ),
                          SizedBox(
                            height: size.height/3,
                            child: Obx((){
                              print(viewController.viewsData.length);
                              if (viewController.viewsData.isEmpty)
                                return Center(child: CircularProgressIndicator());
                              else
                                return ListView.builder(
                                  itemCount: viewController.viewsData.length,
                                  itemBuilder: (BuildContext context, int index) {
                                    return viewTile(dp: viewController.viewsData[index].dp,
                                      firstname: viewController.viewsData[index].firstname,
                                      surname: viewController.viewsData[index].surname,
                                      username:viewController.viewsData[index].username ,
                                      isFollowing: viewController.viewsData[index].isFollowing,
                                      profileId: viewController.viewsData[index].profileId,
                                    );
                                  },
                                );
                            }
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                isScrollControlled: true,
              );

            },

          child :Align(alignment: Alignment.center,
              child: Icon(CupertinoIcons.chevron_compact_up,size: 30,color: Colors.deepOrange,)),

          ),
          Align(alignment: Alignment.center,
              child: Icon(CupertinoIcons.eye,size: 30,color: Colors.deepOrange,)),
          SizedBox(height: 30,),



        ],
      )
    );
  }
}


class viewTile extends StatelessWidget {



 final int profileId;
 final String firstname;
 final String surname;
 final String username;
 final String dp;
 final bool isFollowing;


  const viewTile(
      { required this.profileId, required this.firstname, required this.surname, required this.username,required this.dp,required this.isFollowing});


  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: 65,
      margin: EdgeInsets.symmetric(horizontal: size.width * 0.025, vertical: 8),
      width: size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [

          Align(
            child: Container(
              height: 50,
              width: 50,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.white60,
                ),
                shape: BoxShape.circle,
                image:
                dp == null
                    ? null
                    :
                DecorationImage(
                    image: NetworkImage(dp),
                    fit: BoxFit.cover
                ),
              ),
            ),
          ),


          Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [

              SizedBox(
                width: size.height * 0.1,
              ),

              Align(child: Text('$firstname $surname',style: TextStyle(
                  color: Colors.black,fontWeight: FontWeight.w600,fontSize: 18
              ),)),


              Align(child: Text(username,style: TextStyle(
                  color: Colors.black.withOpacity(.3),fontWeight: FontWeight.w500,fontSize: 16
              ),))
            ],
          ),


          SizedBox(
            width: size.width * 0.09,
          ),

         Align(
           alignment: Alignment.centerRight,
           child:FollowButton(isFollowing:isFollowing),

         )



        ],
      ),
    );
  }
}



class FollowButton extends StatelessWidget {

  final bool isFollowing;
  const FollowButton({
    required this.isFollowing,
  }) ;
  @override
  Widget build(BuildContext context) {
    return isFollowing?


    OutlinedButton(
      child: Text("FOLLOWING"),
      onPressed: () => print("it's pressed"),
      style: ElevatedButton.styleFrom(
        side: BorderSide(width: 2.0, color: Colors.deepOrange),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(32.0),
        ),
      ),
    )

         : ElevatedButton(
           child: Text("FOLLOW"),
           onPressed: () {

           },
           style: ElevatedButton.styleFrom(
           primary: Colors.deepOrange,
           onPrimary: Colors.white,
           shape: RoundedRectangleBorder(
             borderRadius: BorderRadius.circular(32.0),
        ),
      ),
    );
  }
}
