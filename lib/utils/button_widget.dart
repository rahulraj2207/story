import 'package:flutter/material.dart';
import 'package:get/get.dart';
SizedBox buttonWidget({
  required String text,
  required double height,
  required double width,
  required double radius,
  required Color color,
  required Function press,
}) {
  return SizedBox(
    height: height,
    width: width,
    child: TextButton(
      style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(color),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(radius),
          ))),
      onPressed: () => press(),
      child: Center(
        child: Text(
          text,
          style: Get.theme.textTheme.button,
        ),
      ),
    ),
  );
}
